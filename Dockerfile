# syntax=docker/dockerfile:1
# Install all dependencies to speed up build time and normalise environment
FROM golang:alpine AS base
# Install base dependencies
# git added to support go vcs stamping
RUN apk --no-cache add \
    build-base \
    protobuf-dev \
    git \
    bash \
    curl

# Copy source files
COPY api /build/api
COPY racing /build/racing
COPY sport /build/sport

# Docker multi-stage build used below, see
# https://docs.docker.com/develop/develop-images/multistage-build/ if unfamiliar
FROM base AS main
WORKDIR /build
COPY build-deps.sh build-runtime.sh generate-protobuf.sh deps.env /build/
# Use a separate RUN command in case below fails (layers from above are cached)
RUN ./build-deps.sh