package db

const (
	racesList = "list"
)
const (
	columnList = "getColumns"
)

func getRaceQueries() map[string]string {
	return map[string]string{
		racesList: `
			SELECT 
				id, 
				meeting_id, 
				name, 
				number, 
				visible, 
				advertised_start_time 
			FROM races
		`,
		columnList: `select name from pragma_table_info('races')`,
	}
}
