package db

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_getRaceQueries(t *testing.T) {
	raceQueries := getRaceQueries()
	// Ensure raceQueries is not empty... possibly other tests relevant here.
	assert.NotEqual(t, 0, len(raceQueries))
}
