package db

import (
	"database/sql"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Get an in-memory SQLite instance (used several times)
func getMemoryDatabase(t *testing.T) *sql.DB {
	var err error
	racingDB, err := sql.Open("sqlite", ":memory:")
	if err != nil {
		t.Error(`Unexpected error when creating sqlite db: `, err)
	}
	return racingDB
}

// Test to ensure the database seed function works correctly
func Test_racesRepo_seed(t *testing.T) {
	var err error
	racingDB := getMemoryDatabase(t)

	r := &racesRepo{db: racingDB}
	r.init.Do(func() {
		// Seed the DB
		err = r.seed()
		if err != nil {
			t.Error(`Unexpected error when seeding: `, err)
		}

		// Check the database has seeded
		rows, err := r.db.Query(`SELECT count(*) from races`)

		if err != nil {
			t.Error(`Error running DB query: `, err)
		}

		rows.Next()

		var output int
		rowError := rows.Scan(&output)
		if rowError != nil {
			t.Error(`Could not scan rows: `, rowError)
		}

		assert.NotEqual(t, 0, output)

	})
}
