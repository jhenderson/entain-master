package db

import (
	"database/sql"
	"errors"
	"git.neds.sh/matty/entain/racing/proto/racing"
	"google.golang.org/protobuf/types/known/timestamppb"
	_ "modernc.org/sqlite"
	"strconv"
	"strings"
	"sync"
	"time"
)

// RacesRepo provides repository access to races.
type RacesRepo interface {
	// Init will initialise our races repository.
	Init() error

	// List will return a list of races.
	List(filter *racing.ListRacesRequestFilter, sort *racing.ListRacesRequestSort) ([]*racing.Race, error)

	// Get returns an individual race by ID.
	Get(id int64) (*racing.Race, error)
}

type racesRepo struct {
	db   *sql.DB
	init sync.Once
}

// NewRacesRepo creates a new races repository.
func NewRacesRepo(db *sql.DB) RacesRepo {
	return &racesRepo{db: db}
}

// Init prepares the race repository dummy data.
func (r *racesRepo) Init() error {
	var err error

	r.init.Do(func() {
		// For test/example purposes, we seed the DB with some dummy races.
		err = r.seed()
	})

	return err
}

func (r *racesRepo) List(filter *racing.ListRacesRequestFilter, sort *racing.ListRacesRequestSort) ([]*racing.Race, error) {
	var (
		err   error
		query string
		args  []interface{}
	)

	query = getRaceQueries()[racesList]

	query, args = r.applyFilter(query, filter)

	query = r.applySort(query, sort)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	return r.scanRaces(rows)
}

func (r *racesRepo) applyFilter(query string, filter *racing.ListRacesRequestFilter) (string, []interface{}) {
	var (
		clauses []string
		args    []interface{}
	)

	if filter == nil {
		return query, args
	}

	if len(filter.MeetingIds) > 0 {
		clauses = append(clauses, "meeting_id IN ("+strings.Repeat("?,", len(filter.MeetingIds)-1)+"?)")

		for _, meetingID := range filter.MeetingIds {
			args = append(args, meetingID)
		}
	}

	if filter.Visible != nil {
		visible := filter.GetVisible()
		clauses = append(clauses, "visible = "+strconv.FormatBool(visible))
	}

	if filter.Id != nil {
		clauses = append(clauses, "id = ?")
		args = append(args, filter.GetId())
	}

	if len(clauses) != 0 {
		query += " WHERE " + strings.Join(clauses, " AND ")
	}

	return query, args
}

func (r *racesRepo) applySort(query string, sort *racing.ListRacesRequestSort) string {
	if sort == nil {
		return query
	}

	// Get column names directly from db, to ensure only allowed
	// columns are filtered (preventing injection, as ORDER BY cannot be parameterized using '?'
	// (in SQLite at least)
	pragmaRows, _ := r.db.Query(getRaceQueries()[columnList], nil)

	columns := make(map[string]bool)
	for pragmaRows.Next() {
		var name string
		if err := pragmaRows.Scan(&name); err != nil {
			return query
		}
		columns[name] = true
	}

	// If columns contains the user-specified sort field, add it to the query
	if _, ok := columns[sort.Field]; ok {
		query = query + ` ORDER BY ` + sort.Field
	}

	// Handle DESC/ASC
	if sort.Order != nil {
		sortOrder := sort.GetOrder()
		if sortOrder == "ASC" || sortOrder == "DESC" {
			query = query + ` ` + sortOrder
		}
	}

	return query
}

func (m *racesRepo) scanRaces(
	rows *sql.Rows,
) ([]*racing.Race, error) {
	var races []*racing.Race

	for rows.Next() {
		var race racing.Race
		var advertisedStart time.Time

		if err := rows.Scan(&race.Id, &race.MeetingId, &race.Name, &race.Number, &race.Visible, &advertisedStart); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}

			return nil, err
		}

		ts := timestamppb.New(advertisedStart)

		race.AdvertisedStartTime = ts

		if advertisedStart.After(time.Now()) {
			race.Status = "OPEN"
		} else {
			race.Status = "CLOSED"
		}

		races = append(races, &race)
	}

	return races, nil
}

func (r *racesRepo) Get(id int64) (*racing.Race, error) {
	filter := &racing.ListRacesRequestFilter{Id: &id}
	races, err := r.List(filter, nil)
	if err != nil {
		return nil, err
	}
	if len(races) == 0 {
		return nil, errors.New("no matching race found with the given ID: " + strconv.FormatInt(id, 10))
	}

	return races[0], nil
}
