package db

import (
	"git.neds.sh/matty/entain/racing/proto/racing"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Ensure NewRacesRepo initialiser works correctly
func TestNewRacesRepo(t *testing.T) {
	racingDB := getMemoryDatabase(t)
	repo := NewRacesRepo(racingDB)
	assert.Equalf(t, repo, NewRacesRepo(racingDB), "NewRacesRepo(%v)", racingDB)
}

// Ensure Init method (and seeding) of database works correctly
func Test_racesRepo_Init(t *testing.T) {
	racingDB := getMemoryDatabase(t)
	repo := NewRacesRepo(racingDB)
	assert.Equal(t, nil, repo.Init())
}

// Ensure racesRepo list is non-empty. Other tests could be added here to test filtered
// list calls.
func Test_racesRepo_List(t *testing.T) {
	racingDB := getMemoryDatabase(t)
	repo := NewRacesRepo(racingDB)
	assert.Equal(t, nil, repo.Init())
	list, err := repo.List(nil, nil)
	if err != nil {
		return
	}
	assert.NotEqual(t, 0, len(list))
}

// Test the applyFilter function
func Test_racesRepo_applyFilter(t *testing.T) {
	racingDB := getMemoryDatabase(t)

	r := &racesRepo{
		db: racingDB,
	}

	err := r.Init()
	if err != nil {
		assert.Error(t, err)
	}

	query := getRaceQueries()[racesList]
	resultQuery, _ := r.applyFilter(query, nil)
	assert.Equal(t, query, resultQuery)
}

func BoolPointer(b bool) *bool {
	return &b
}

// Test the applyFilter visible function
func Test_racesRepo_applyVisibleFilter(t *testing.T) {
	racingDB := getMemoryDatabase(t)

	r := &racesRepo{
		db: racingDB,
	}

	err := r.Init()
	if err != nil {
		assert.Error(t, err)
	}

	query := getRaceQueries()[racesList]
	filter := &racing.ListRacesRequestFilter{Visible: BoolPointer(true)}
	resultQuery, _ := r.applyFilter(query, filter)

	// Ensure the query has been adjusted to include Visible = true
	// Note, explicitly checking for 'visible = true' is a bit overkill, so
	// just checking the query has mutated is enough for the sake of this
	// unit test.
	assert.NotEqual(t, query, resultQuery)
}

// Test scanRaces function
func Test_racesRepo_scanRaces(t *testing.T) {
	racingDB := getMemoryDatabase(t)

	r := &racesRepo{
		db: racingDB,
	}

	err := r.Init()
	if err != nil {
		assert.Error(t, err)
	}

	query := getRaceQueries()[racesList]

	rows, err := r.db.Query(query)
	if err != nil {
		assert.Error(t, err)
	}

	resultQuery, _ := r.scanRaces(rows)
	assert.NotEqual(t, 0, len(resultQuery))
}
