package main

import (
	"database/sql"
	"flag"
	"log"
	"net"

	"git.neds.sh/matty/entain/racing/db"
	"git.neds.sh/matty/entain/racing/proto/racing"
	"git.neds.sh/matty/entain/racing/service"
	"google.golang.org/grpc"
)

var (
	grpcEndpoint = flag.String("grpc-endpoint", "localhost:9000", "gRPC server endpoint")
)

func main() {
	flag.Parse()

	conn, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("failed opening tcp socket: %s\n", err)
		return
	}

	grpcServer, err := run()

	if err != nil {
		log.Fatalf("failed running grpc server: %s\n", err)
		return
	}

	log.Printf("gRPC (Racing) server listening on: %s\n", *grpcEndpoint)

	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalf("failed running grpc server: %s\n", err)
		return
	}

}

func run() (*grpc.Server, error) {
	racingDB, err := sql.Open("sqlite", "./db/racing.db")
	if err != nil {
		return nil, err
	}

	racesRepo := db.NewRacesRepo(racingDB)
	if err := racesRepo.Init(); err != nil {
		return nil, err
	}

	grpcServer := grpc.NewServer()

	racing.RegisterRacingServer(
		grpcServer,
		service.NewRacingService(
			racesRepo,
		),
	)

	return grpcServer, nil
}
