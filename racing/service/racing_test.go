package service

import (
	"database/sql"
	"git.neds.sh/matty/entain/racing/db"
	"git.neds.sh/matty/entain/racing/proto/racing"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Get an in-memory SQLite instance (used several times)
func getMemoryDatabase(t *testing.T) *sql.DB {
	var err error
	racingDB, err := sql.Open("sqlite", ":memory:")
	if err != nil {
		t.Error(`Unexpected error when creating sqlite db: `, err)
	}
	return racingDB
}

func TestNewRacingService(t *testing.T) {
	racingDB := getMemoryDatabase(t)
	repo := db.NewRacesRepo(racingDB)
	assert.NotEqual(t, nil, NewRacingService(repo))
}

func Test_racingService_ListRaces(t *testing.T) {
	racingDB := getMemoryDatabase(t)
	repo := db.NewRacesRepo(racingDB)
	err := repo.Init()
	if err != nil {
		return
	}
	service := NewRacingService(repo)
	racesRequest := new(racing.ListRacesRequest)
	races, err := service.ListRaces(nil, racesRequest)
	if err != nil {
		t.Error(`Unexpected error when calling ListRaces service: `, err)
	}
	assert.NotEqual(t, 0, len(races.Races))
}
