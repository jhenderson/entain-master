#!/bin/bash
# Start both api and sport servers in the background as child scripts to start.sh.
# Useful for debugging/testing/local servers. Should not be used in production; each
# server should be started as a daemon/standalone container.

# handle INT signal for ctrl-c termination
trap ctrl_c INT

function ctrl_c() {
  kill $API_PID
  kill $RACING_PID
  kill $SPORT_PID
  echo "ending..."
}


cd racing
./racing &
RACING_PID=$!
cd ..

cd sport
./sport &
SPORT_PID=$!
cd ..

cd api
./api &
API_PID=$!
cd ..

wait $API_PID 2>/dev/null
wait $RACING_PID 2>/dev/null
wait $SPORT_PID 2>/dev/null