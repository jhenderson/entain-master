package main

import (
	"context"
	"net/http"
	"testing"
	"time"
)

// Test the run function to ensure a HTTP server is started
func TestRunServer(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	server := run(ctx)

	// Set up a 1-second shutdown handler
	go func() {
		time.Sleep(1 * time.Second)
		err := server.Shutdown(ctx)
		if err != nil {
			t.Error(`HTTP server did not shut down gracefully, error: `, err)
		}
	}()

	err := server.ListenAndServe()
	if err != http.ErrServerClosed {
		t.Error(`HTTP server did not start correctly, error: `, err)
	}
}
