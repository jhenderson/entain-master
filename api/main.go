package main

import (
	"context"
	"flag"
	"git.neds.sh/matty/entain/api/proto/racing"
	"git.neds.sh/matty/entain/api/proto/sport"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
)

var (
	apiEndpoint   = flag.String("api-endpoint", "localhost:8000", "API endpoint")
	grpcEndpoint  = flag.String("grpc-endpoint", "localhost:9000", "gRPC server endpoint")
	sportEndpoint = flag.String("sport-endpoint", "localhost:9001", "sport gRPC server endpoint")
)

func main() {
	flag.Parse()
	// Move context to main function to ensure defer runs at top level
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	server := run(ctx)
	err := server.ListenAndServe()
	if err != nil {
		log.Printf("failed running api server: %s\n", err)
	}
}

func run(ctx context.Context) *http.Server {
	mux := runtime.NewServeMux()
	if err := racing.RegisterRacingHandlerFromEndpoint(
		ctx,
		mux,
		*grpcEndpoint,
		[]grpc.DialOption{grpc.WithInsecure()},
	); err != nil {
		return nil
	}

	if err := sport.RegisterSportHandlerFromEndpoint(
		ctx,
		mux,
		*sportEndpoint,
		[]grpc.DialOption{grpc.WithInsecure()},
	); err != nil {
		return nil
	}

	log.Printf("API server listening on: %s\n", *apiEndpoint)

	// Create and return a server instance to allow graceful test shutdown
	server := &http.Server{
		Addr:    *apiEndpoint,
		Handler: mux,
		BaseContext: func(l net.Listener) context.Context {
			return ctx
		},
	}

	return server
}
