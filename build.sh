#!/bin/bash
# Build script for entain-master, this produces local development/native binaries.
# Clean existing outputs
rm -f api/api
rm -f racing/racing
rm -f sport/sport

# Build the docker image, then use it to create a native binary in dist
# this ensures the binary is built in the exact same manner as the CI (assuming
# docker version is valid).
docker build -t entain-master .

# Inject the current platform name (ie. darwin on OSX).
BUILDOS=$(uname)
docker run \
  -v $(pwd)/api:/build/api \
  -v $(pwd)/racing:/build/racing \
  -v $(pwd)/sport:/build/sport \
  -it entain-master ./build-runtime.sh $BUILDOS
