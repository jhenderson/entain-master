#!/bin/bash
# Install go dependencies for each module within the project. This should be done within docker to ensure
# a consistent build environment with the CI.
# Usage: build-deps.sh
export PATH="$PATH:$(go env GOPATH)/bin"
DEPS=`cat $(pwd)/deps.env`
for module in */go.mod ; do
  cd $(dirname $module)
  go install $DEPS
  cd ..
done