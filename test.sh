#!/bin/bash
export PATH="$PATH:$(go env GOPATH)/bin"
for module in */go.mod ; do
  cd $(dirname $module)
  go test -v -vet=all ./...
  cd ..
done