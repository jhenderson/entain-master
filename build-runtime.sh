#!/bin/bash
# Build the Go runtime itself. This should be done within docker to ensure
# a consistent build environment with the CI, but can be run locally with no arguments.
# Usage: build-runtime.sh [platform]
export PATH="$PATH:$(go env GOPATH)/bin"
if [[ ! -z "${1}" ]]; then
  export GOOS=${1,,}
fi
# Ensure protobuf files are generated
./generate-protobuf.sh

# Build all project modules
for module in */go.mod ; do
  cd $(dirname $module)
  go build
  cd ..
done