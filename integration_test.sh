#!/bin/bash
export PATH="$PATH:$(go env GOPATH)/bin"
# Start both api and sport servers in the background as child scripts to start.sh.
# Useful for debugging/testing/local servers. Should not be used in production; each
# server should be started as a daemon/standalone container.

./start.sh &
SERVER_PID=$!

sleep 2
DATA=$(curl -X "POST" "http://localhost:8000/v1/list-races" \
     -H 'Content-Type: application/json' \
     -d $'{
  "filter": {}
}')

DATA_SPORTS=$(curl -X "POST" "http://localhost:8000/v1/list-events" \
     -H 'Content-Type: application/json' \
     -d $'{
  "filter": {}
}')

echo $DATA
echo $DATA_SPORTS

# Below function from https://unix.stackexchange.com/questions/440691/killing-parent-process-doesnt-kill-child
# Kill all spawned child processes
function kill_recurse() {
    cpids=`pgrep -P $1|xargs`
    for cpid in $cpids;
    do
        kill_recurse $cpid
    done
    echo "killing $1"
    kill -9 $1
}

kill_recurse $SERVER_PID

EXPECTED='{"races":['
if [[ ! "$DATA" == *"$EXPECTED"* ]]; then
  exit 1
fi

EXPECTED_SPORTS='{"events":['
if [[ ! "$DATA_SPORTS" == *"$EXPECTED_SPORTS"* ]]; then
  exit 1
fi