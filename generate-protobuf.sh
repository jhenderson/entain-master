#!/bin/bash

# This ensures all go protobuf dependencies are installed
./build-deps.sh

export PATH="$PATH:$(go env GOPATH)/bin"
# Generates the .pb protobuf files in all go modules
for module in */go.mod ; do
  cd $(dirname $module)
  go generate ./...
  cd ..
done