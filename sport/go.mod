module git.neds.sh/matty/entain/sport

go 1.16

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.11.3
	github.com/stretchr/testify v1.8.0 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e
	google.golang.org/genproto v0.0.0-20220914210030-581e60b4ef85
	google.golang.org/grpc v1.49.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.2.0
	google.golang.org/protobuf v1.28.1
	modernc.org/sqlite v1.18.2 // indirect
	syreclabs.com/go/faker v1.2.3
)
