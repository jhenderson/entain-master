package main

import (
	"github.com/stretchr/testify/assert"
	"net"
	"testing"
	"time"
)

func Test_run(t *testing.T) {
	server, err := run()
	go func() {
		time.Sleep(1 * time.Second)
		server.GracefulStop()
	}()

	conn, err := net.Listen("tcp", ":9000")
	if err != nil {
		assert.Error(t, err)
	}

	err = server.Serve(conn)
	if err != nil {
		assert.Error(t, err)
	}
}
