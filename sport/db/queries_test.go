package db

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_getSportsQueries(t *testing.T) {
	sportsQueries := getSportsQueries()
	// Ensure sportsQueries is not empty... possibly other tests relevant here.
	assert.NotEqual(t, 0, len(sportsQueries))
}
