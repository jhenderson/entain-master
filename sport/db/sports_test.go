package db

import (
	"git.neds.sh/matty/entain/sport/proto/sport"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Ensure NewSportRepo initialiser works correctly
func TestNewSportRepo(t *testing.T) {
	sportDB := getMemoryDatabase(t)
	repo := NewSportRepo(sportDB)
	assert.Equalf(t, repo, NewSportRepo(sportDB), "NewSportRepo(%v)", sportDB)
}

// Ensure Init method (and seeding) of database works correctly
func Test_sportRepo_Init(t *testing.T) {
	sportDB := getMemoryDatabase(t)
	repo := NewSportRepo(sportDB)
	assert.Equal(t, nil, repo.Init())
}

// Ensure sportRepo list is non-empty. Other tests could be added here to test filtered
// list calls.
func Test_sportRepo_List(t *testing.T) {
	sportDB := getMemoryDatabase(t)
	repo := NewSportRepo(sportDB)
	assert.Equal(t, nil, repo.Init())
	list, err := repo.List(nil, nil)
	if err != nil {
		return
	}
	assert.NotEqual(t, 0, len(list))
}

// Test the applyFilter function
func Test_sportRepo_applyFilter(t *testing.T) {
	sportDB := getMemoryDatabase(t)

	r := &sportRepo{
		db: sportDB,
	}

	err := r.Init()
	if err != nil {
		assert.Error(t, err)
	}

	query := getSportsQueries()[eventsList]
	resultQuery, _ := r.applyFilter(query, nil)
	assert.Equal(t, query, resultQuery)
}

func BoolPointer(b bool) *bool {
	return &b
}

// Test the applyFilter visible function
func Test_sportRepo_applyVisibleFilter(t *testing.T) {
	sportDB := getMemoryDatabase(t)

	r := &sportRepo{
		db: sportDB,
	}

	err := r.Init()
	if err != nil {
		assert.Error(t, err)
	}

	query := getSportsQueries()[eventsList]
	filter := &sport.ListEventsRequestFilter{Visible: BoolPointer(true)}
	resultQuery, _ := r.applyFilter(query, filter)

	// Ensure the query has been adjusted to include Visible = true
	// Note, explicitly checking for 'visible = true' is a bit overkill, so
	// just checking the query has mutated is enough for the sake of this
	// unit test.
	assert.NotEqual(t, query, resultQuery)
}

// Test scanRaces function
func Test_sportRepo_scanEvents(t *testing.T) {
	sportDB := getMemoryDatabase(t)

	r := &sportRepo{
		db: sportDB,
	}

	err := r.Init()
	if err != nil {
		assert.Error(t, err)
	}

	query := getSportsQueries()[eventsList]

	rows, err := r.db.Query(query)
	if err != nil {
		assert.Error(t, err)
	}

	resultQuery, _ := r.scanEvents(rows)
	assert.NotEqual(t, 0, len(resultQuery))
}
