package db

const (
	eventsList = "list"
)
const (
	columnList = "getColumns"
)

func getSportsQueries() map[string]string {
	return map[string]string{
		eventsList: `
			SELECT 
				id,
				name, 
				visible, 
				advertised_start_time 
			FROM events
		`,
		columnList: `select name from pragma_table_info('events')`,
	}
}
