package db

import (
	"database/sql"
	"git.neds.sh/matty/entain/sport/proto/sport"
	"google.golang.org/protobuf/types/known/timestamppb"
	_ "modernc.org/sqlite"
	"strconv"
	"strings"
	"sync"
	"time"
)

// SportRepo provides repository access to sport events.
type SportRepo interface {
	// Init will initialise our sport repository.
	Init() error

	// List will return a list of sport events.
	List(filter *sport.ListEventsRequestFilter, sort *sport.ListEventsRequestSort) ([]*sport.Event, error)
}

type sportRepo struct {
	db   *sql.DB
	init sync.Once
}

// NewSportRepo creates a new sport repository.
func NewSportRepo(db *sql.DB) SportRepo {
	return &sportRepo{db: db}
}

// Init prepares the sport repository dummy data.
func (r *sportRepo) Init() error {
	var err error

	r.init.Do(func() {
		// For test/example purposes, we seed the DB with some dummy sports.
		err = r.seed()
	})

	return err
}

func (r *sportRepo) List(filter *sport.ListEventsRequestFilter, sort *sport.ListEventsRequestSort) ([]*sport.Event, error) {
	var (
		err   error
		query string
		args  []interface{}
	)

	query = getSportsQueries()[eventsList]

	query, args = r.applyFilter(query, filter)

	query = r.applySort(query, sort)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	return r.scanEvents(rows)
}

func (r *sportRepo) applyFilter(query string, filter *sport.ListEventsRequestFilter) (string, []interface{}) {
	var (
		clauses []string
		args    []interface{}
	)

	if filter == nil {
		return query, args
	}

	if filter.Visible != nil {
		visible := filter.GetVisible()
		clauses = append(clauses, "visible = "+strconv.FormatBool(visible))
	}

	if filter.Id != nil {
		clauses = append(clauses, "id = ?")
		args = append(args, filter.GetId())
	}

	if len(clauses) != 0 {
		query += " WHERE " + strings.Join(clauses, " AND ")
	}

	return query, args
}

func (r *sportRepo) applySort(query string, sort *sport.ListEventsRequestSort) string {
	if sort == nil {
		return query
	}

	// Get column names directly from db, to ensure only allowed
	// columns are filtered (preventing injection, as ORDER BY cannot be parameterized using '?'
	// (in SQLite at least)
	pragmaRows, _ := r.db.Query(getSportsQueries()[columnList], nil)

	columns := make(map[string]bool)
	for pragmaRows.Next() {
		var name string
		if err := pragmaRows.Scan(&name); err != nil {
			return query
		}
		columns[name] = true
	}

	// If columns contains the user-specified sort field, add it to the query
	if _, ok := columns[sort.Field]; ok {
		query = query + ` ORDER BY ` + sort.Field
	}

	// Handle DESC/ASC
	if sort.Order != nil {
		sortOrder := sort.GetOrder()
		if sortOrder == "ASC" || sortOrder == "DESC" {
			query = query + ` ` + sortOrder
		}
	}

	return query
}

func (m *sportRepo) scanEvents(
	rows *sql.Rows,
) ([]*sport.Event, error) {
	var events []*sport.Event

	for rows.Next() {
		var event sport.Event
		var advertisedStart time.Time

		if err := rows.Scan(&event.Id, &event.Name, &event.Visible, &advertisedStart); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}

			return nil, err
		}

		ts := timestamppb.New(advertisedStart)

		event.AdvertisedStartTime = ts

		if advertisedStart.After(time.Now()) {
			event.Status = "OPEN"
		} else {
			event.Status = "CLOSED"
		}

		events = append(events, &event)
	}

	return events, nil
}
