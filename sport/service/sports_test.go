package service

import (
	"database/sql"
	"git.neds.sh/matty/entain/sport/db"
	"git.neds.sh/matty/entain/sport/proto/sport"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Get an in-memory SQLite instance (used several times)
func getMemoryDatabase(t *testing.T) *sql.DB {
	var err error
	sportDB, err := sql.Open("sqlite", ":memory:")
	if err != nil {
		t.Error(`Unexpected error when creating sqlite db: `, err)
	}
	return sportDB
}

func TestNewSportService(t *testing.T) {
	sportDB := getMemoryDatabase(t)
	repo := db.NewSportRepo(sportDB)
	assert.NotEqual(t, nil, NewSportService(repo))
}

func Test_sportService_ListEvents(t *testing.T) {
	sportDB := getMemoryDatabase(t)
	repo := db.NewSportRepo(sportDB)
	err := repo.Init()
	if err != nil {
		return
	}
	service := NewSportService(repo)
	eventsRequest := new(sport.ListEventsRequest)
	events, err := service.ListEvents(nil, eventsRequest)
	if err != nil {
		t.Error(`Unexpected error when calling ListEvents service: `, err)
	}
	assert.NotEqual(t, 0, len(events.Events))
}
