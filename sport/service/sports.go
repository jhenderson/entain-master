package service

import (
	"git.neds.sh/matty/entain/sport/db"
	"git.neds.sh/matty/entain/sport/proto/sport"
	"golang.org/x/net/context"
)

type Sports interface {
	// ListEvents will return a collection of events.
	ListEvents(ctx context.Context, in *sport.ListEventsRequest) (*sport.ListEventsResponse, error)
}

// sportService implements the Sports interface.
type sportService struct {
	sportRepo db.SportRepo
}

// NewSportService instantiates and returns a new sportService.
func NewSportService(sportRepo db.SportRepo) Sports {
	return &sportService{sportRepo}
}

func (s *sportService) ListEvents(ctx context.Context, in *sport.ListEventsRequest) (*sport.ListEventsResponse, error) {
	events, err := s.sportRepo.List(in.Filter, in.Sort)
	if err != nil {
		return nil, err
	}

	return &sport.ListEventsResponse{Events: events}, nil
}
