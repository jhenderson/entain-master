package main

import (
	"database/sql"
	"flag"
	"git.neds.sh/matty/entain/sport/proto/sport"
	"log"
	"net"

	"git.neds.sh/matty/entain/sport/db"
	"git.neds.sh/matty/entain/sport/service"
	"google.golang.org/grpc"
)

var (
	grpcEndpoint = flag.String("grpc-endpoint", "localhost:9001", "gRPC server endpoint")
)

func main() {
	flag.Parse()

	conn, err := net.Listen("tcp", ":9001")
	if err != nil {
		log.Fatalf("failed opening tcp socket: %s\n", err)
		return
	}

	grpcServer, err := run()

	if err != nil {
		log.Fatalf("failed running grpc server: %s\n", err)
		return
	}

	log.Printf("gRPC (Sport) server listening on: %s\n", *grpcEndpoint)

	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalf("failed running grpc server: %s\n", err)
		return
	}

}

func run() (*grpc.Server, error) {
	sportDB, err := sql.Open("sqlite", "./db/sport.db")
	if err != nil {
		return nil, err
	}

	sportRepo := db.NewSportRepo(sportDB)
	if err := sportRepo.Init(); err != nil {
		return nil, err
	}

	grpcServer := grpc.NewServer()

	sport.RegisterSportServer(
		grpcServer,
		service.NewSportService(
			sportRepo,
		),
	)

	return grpcServer, nil
}
